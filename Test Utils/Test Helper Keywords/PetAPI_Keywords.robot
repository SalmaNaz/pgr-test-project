*** Settings ***
Library    REST
Library   JSONLibrary
Library    BuiltIn
Library     Collections
Resource    ../../RestAPIHelper.robot
Resource    ../CommonVariables.robot

*** Variables ***
&{Parameters}               wcsId=6     petname=4   microchip=11
${RequestJsonFilepath}      ${envDetails}[PETAPICREATEREQUESTJSONPATH]
${ResponseJsonFilepath}     ${envDetails}[PETAPICREATERESPONSEJSONPATH]
@{shouldExistresponseParameterList}     ${jsonParamPetMasterId}      ${jsonParamPetName}

*** Keywords ***

# Generates Pet API data for payload
# Arguments: 9 optionl parameters set to null by default. When null random data is generated and when value is passed the value passed is set
# Returns request data dictionary which have the generated data for pet parent Id, pet name and micro chio number.
Generate Pet API Data
    [Arguments]    ${petParentId}=null   ${petName}=null    ${petColor}=null   ${petType}=null    ${petBreed}=null   ${petGender}=null     ${petWeight}=null     ${petDeceasedFlag}=null       ${petNeuteredFlag}=null

    log to console  The pet data is generated with pet parent Id having &{Parameters}[wcsId] numbers, petname having &{Parameters}[petname] letters and microchipnum having &{Parameters}[microchip] numbers

    # Set random Pet parent Id or what is passed in arguments
    ${petParentId}=     run keyword if  '${petParentId}' == 'null'    Set Pet Parent Id      wcsId      &{Parameters}[wcsId]    [NUMBER]
    ...          ELSE        set variable  ${petParentId}
    log to console  The value set to pet parent Id is:${petParentId}

     # Add the pet parent Id values to dictionary for validations
     ${requestDataDict}=              Create Dictionary
     Set To Dictionary    ${requestDataDict}   ${petParentId_jsonPath}  ${petParentId}

    # Set random Pet name or what is passed in arguments
    ${petName}=     run keyword if  '${petName}' == 'null'     set pet name    petname      &{Parameters}[petname]    [LOWER]
    ...          ELSE        set variable  ${petName}
    log to console  The value set to pet name is:${petName}

    # Add the pet name values to dictionary for validations
    Set To Dictionary    ${requestDataDict}    ${petName_jsonPath}   ${petName}

    # Set random micro chip number
    ${petMicrochipnum}     Generate RandomData     microchipname   &{Parameters}[microchip]  [NUMBERS]
    log to console    Prefix the microchipnumber with with 1
    ${petMicrochipnum}     Set variable        1${petMicrochipnum}
    log to console  The value set to microchip number is:${petMicrochipnum}

    # Add the microchipnumber values to dictionary for validations
    Set To Dictionary    ${requestDataDict}    ${petMicrochipnumber_jsonPath}   ${petMicrochipnum}

     ${petType}=    Run Keyword If   '${petType}' == 'null'    set variable  Dog
    ...     ELSE  set variable  ${petType}
    Set To Dictionary    ${requestDataDict}    ${petType_jsonpath}   ${petType}
    log to console  The value set to pet type is:${petType}

     ${petBreed}=    Run Keyword If   '${petBreed}' == 'null'    set variable  Boxer
    ...     ELSE  set variable  ${petBreed}
    Set To Dictionary    ${requestDataDict}    ${petBreed_jsonpath}   ${petBreed}
    log to console  The value set to pet Breed is:${petBreed}

    Run Keyword If   '${petGender}' == 'null'           Log to console      The value set to pet gender is Male
    ${petGender}      Set variable        Male

    ${petColor}=    Run Keyword If   '${petColor}' == 'null'    set variable  Brown
    ...     ELSE  set variable  ${petColor}
    Set To Dictionary    ${requestDataDict}    ${petColor_jsonpath}   ${petColor}
    log to console  The value set to pet color is:${petColor}

    Run Keyword If   '${petWeight}' == 'null'           Log to console      The value set to pet weight is 30
    ${petWeight}      Set variable        30

    Run Keyword If   '${petDeceasedFlag}' != 'null'     Log to console     The value set to pet deceased flag is N
     ${petDeceasedFlag}    Set variable        N

    Run Keyword If   '${petNeuteredFlag}' != 'null'    log to console     The value set to pet neutered flag is N
    ${petNeuteredFlag}   Set variable        N

    [Return]  &{requestDataDict}

# Sets Random Pet Parent Id and also prefixes it with text PGRPETPARENT
# Arguments: 3 parameters: 1-->data for which field, 2-->what length of data, 3-->what characters of data
# Returns pet parent Id prefixed with PGRPETPARENT
Set Pet Parent Id
    [Arguments]  ${datafor}   ${length}  ${charecters}
    ${petParentId}  Generate RandomData   ${datafor}   ${length}  ${charecters}

    log to console  Prefix the pet parent Id with PGRPETPARENT
    ${petParentId}    Set variable        PGRPETPARENT${petParentId}
    [Return]  ${petParentId}

# Sets Random Pet Name and also prefixes it with text Pgrtestpet
# Arguments: 3 parameters: 1-->data for which field, 2-->what length of data, 3-->what characters of data
# Returns pet name prefixed with PGRTESTPET
Set Pet name
    [Arguments]  ${datafor}   ${length}  ${charecters}
    ${petName}   Generate RandomData   ${datafor}   ${length}  ${charecters}

    log to console     Prefix the pet name with PGRTESTPET
    ${petName}    Set variable        PGRTESTPET${petName}
    [Return]  ${petName}


# Generates the random data or sets the user sent data and validates the response when input in Invalid, Builds the json request based on data generate pet api data, Posts the request and validates respose code and response parameters and values.
# Arguments: 2 required parameters and 11 optional parameters.
# Parameter 1(required)-->expected status code, 2(required)-->what parameter to be validated in response
# Parameter 3 to 11 are optional parameters which are set to null by default and when user passes the data it will be set with user passed data.
# Parameter 3: expected value of parameter in response, if user sent this will be passed in test case and if random data it will taken from dictionary.
# Parameter 4-->apikey, 5 to 13 are parent Id, pet name, pet color, pet type, breed, gender, weight, deceasedflag, neutered flag.
GeneratePetAPIData SubmitRequest And Validate Valid Response
    [Arguments]     ${expectedStatusCode}   ${validateParameter}    ${ValidateParameterValue}=null  ${deleteParameter}=null      ${shouldNotExistParameter}=null     ${apikey}=&{apikeyDict}[mobile]     ${petParentId}=null   ${petName}=null    ${petColor}=null   ${petType}=null    ${petBreed}=null   ${petGender}=null     ${petWeight}=null     ${petDeceasedFlag}=null       ${petNeuteredFlag}=null

    # Generate the pet data and store the required data in dictionary and return the dictionary.
    ${updateRequestDict}   Generate Pet API Data  ${petParentId}  ${petName}  ${petColor}  ${petType}  ${petBreed}  ${petGender} ${petWeight}   ${petDeceasedFlag}    ${petNeuteredFlag}

    # Build the json request using the dictionary and return request json.
    ${requestJson}   Build Request Json    ${requestjsonfilepath}      ${updateRequestDict}     ${deleteParameter}

    # Post the request json and validate the response code, save the json to file and return the response content.
    ${response}     Post REST Request And Validate Response Code   ${apikey}  ${petAPICreateAlias}   ${envDetails}[${BaseURL}]      ${envDetails}[${PetURI}]    ${requestJson}   ${ResponseJsonFilepath}   ${expectedStatusCode}

   run keyword if    '${expectedStatusCode}' == '200'       Validate Valid Post Request Response Parameters and Values  ${ResponseJsonFilepath}     ${validateParameter}        ${updateRequestDict}[${validateParameter}]      ${shouldNotExistParameter}      @{shouldExistresponseParameterList}
    ...     ELSE    Validate Valid Post Request Response Parameters and Values  ${ResponseJsonFilepath}     ${validateParameter}        ${ValidateParameterValue}     ${shouldNotExistParameter}

