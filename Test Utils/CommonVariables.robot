*** Settings ***
Library     BuiltIn
Library     String
Variables   ../Test Configurations/EnvironmentSetUpUtil.py


*** Variables ***
# Test Execution Variables  Initialization

${envDetails}   ${getenvironmentdetails('${env}')}
&{apikeyDict}=  mobile=${envDetails}[MOBILEAPIKEY]  ecomm=${envDetails}[ECOMMAPIKEY]

# Constant String variables
${petAPICreateAlias}        CreatePetAPI
${petAPICreateAlias}        CreateIndividualAPI

# Dictionary Key Constants
${BaseURL}  BASEURL
${PetURI}   PETURI


#   Json Files Parameter Paths
${petParentId_jsonPath}   $..pet_parent..pet_parent_id
${petName_jsonPath}   $..name
${petMicrochipNumber_jsonPath}  $..microchip
${petColor_jsonpath}            $..pet_color
${petNeuteredFlag_jsonpath}     $..neutered
${petType_jsonpath}             $..pet_type
${petBreed_jsonpath}            $..primary_breed
${petGender_jsonpath}           $..gender

# Status Codes
${ResponseCodeOK}                     200
${ResponseCodeBadRequest}             400
${ResponseCodeNotFound}               404


# Json File Parameters
${jsonParamPetMasterId}  pet_master_id
${jsonParamPetName}      name
