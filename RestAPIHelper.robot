*** Settings ***
Library     BuiltIn
Library     String
Resource    ./Test Utils/CommonVariables.robot
Library     OperatingSystem
Library     Collections
Library     JSONLibrary
Library     REST
Library     RequestsLibrary

*** Variables ***

*** Keywords ***
# ****************** REST API HELPER METHODS ************************************* #

# Generates Random data
# Arguments: 3 parameters: 1-->data for which field, 2-->what length of data, 3-->what characters of data
# Returns specified random data.
Generate RandomData
    [Arguments]     ${datafor}    ${length}      ${chars}
   ${randomData}    Generate Random String   ${length}    ${chars}
   Log  "The Random Data ${randomData} is generated for": ${datafor}
   [Return]  ${randomData}

# Builds the header
# Arguments: 1 parameter 1-->api key which is optional and is set to mobile api key by default
# Returns the header as dictionary
Build Request Header
    [Arguments]     ${apikey}=&{apikeyDict}[mobile]
    &{Header}   Create Dictionary       Content-Type=application/json   x-api-key=${apikey}    Accept=application/json
    [Return]    &{Header}

# Builds the Json request
# Arguments: two parameters: 1-->the filepath to sample json request file, 2-->dictionary which contains json parameter and value with which the request json is updated
# Returns: the Json created.
Build Request Json
    [Arguments]     ${RequestJsonFilepath}   ${requestUpdateDataDict}   ${deleteParameter}=null

    ${createRequestJson}      Load JSON From File   ${RequestJsonFilepath}
    log to console      \nThe data with which the request Json will be updated is:${requestUpdateDataDict}

    ${items}   Get Dictionary Items           ${requestUpdateDataDict}
    :FOR    ${key}    ${value}    IN    @{items}
    \    ${createRequestJson}      Update Value To Json        ${createRequestJson}       ${key}          ${value}

    # Delete the attribute from request if any provided
    run keyword if  '${deleteParameter}' == 'null'   log to console  There is no parameter to delete from request
    ...     ELSE    Delete Object From Json   ${createRequestJson}    ${deleteParameter}
     log to console      \nThe attribute deleted from json is:${deleteParameter}

    log to console  \nThe Json data which will be used in payload is:${createRequestJson}

    [Return]  ${createRequestJson}

# Posts the Json request and Valdiates the response status
# Arguments: 7 parameters:
# 1-->api key Ex: api key for mobile or ecomm etc, 2-->the alias for API  url, 3-->the base url Ex: https://ckservices-perf.petc.com/ckapi-team-dev-V2-27
# 4-->the relativeurl Ex: /pgr/pets, 5-->the request to post 6-->the filepath for response to save, 7-->expected response code.
# Returns: resonse content in json.
Post REST Request And Validate Response Code
    [Arguments]     ${apiKey}   ${alias}    ${baseUrl}  ${relativeUrl}  ${requestToPost}  ${ResponseJsonFilepath}   ${ExpectedResponseCode}
    log to console  \nPayload is posted at:${baseUrl}${relativeUrl}

    Create Session      ${alias}   ${baseUrl}   disable_warnings=1
    ${Header}   Build Request Header  ${apiKey}
    ${response}    Post Request    ${alias}    ${relativeUrl}  headers=${Header}    data=${requestToPost}

     # Console the response
     log to console  \nThe response is:${response.content}

     # Validate the response code is equal.
     log to console  \nThe actual response code is:${response.status_code}
     log to console  \nThe expected response code is:${ExpectedResponseCode}
     should be equal as strings  ${ExpectedResponseCode}  ${response.status_code}   "The response code is not as expected.Expected:"${ExpectedResponseCode}"Actual:"${response.status_code}

     # Save the response to file for further validations.
     Save Response to File   ${ResponseJsonFilepath}   ${response.content}
     [Return]  ${response.content}

# Saves the response to File
# Arguments: 2, Parameter 1-->filepath for response to save, 2-->the response in json.
Save Response to File
    [Arguments]         ${ResponseJsonFilepath}     ${response}
    ${responseToFile}     set variable    ${response}
    ${responseToFile}   convert to string   ${responseToFile}
    Create File          ${ResponseJsonFilepath}       ${responseToFile}

# Validate the parameters(ValidateObject) and values of parameters(expectedobjectvalue) in response.
# Arguments: 6, Parameters 1-->the response json path, 2-->the expected status code, 3-->what parameter in resonse to be validated liek name, pet master id etc
# 4-->the valus of parameter Ex: the value of name, pet master Id etc, 5-->the parameters list, 6-->the parameter should contain or nor in response, true or false.
Validate Response Data
    [Arguments]  ${responseJsonPath}  ${ValidateObject}   ${ExpectedObjectValue}    @{responseValuesList}       ${shouldContain}=true       ${specificParameterExists}=true

    # Load response from file to variable.
    ${Json_response}      Load Json From File        ${responseJsonPath}
    log to console  "The response loaded from file is:"${Json_response}

    # Get the data to list to validate parameter contains in response.
    @{MyList}=  convert to list     ${Json_response}

    # Validate parameter exists in response.
    :FOR    ${item}    IN    @{responseValuesList}
    \  run keyword if  '${shouldContain}' == 'true'  Should Contain    @{MyList}   ${item}   log to console  "The request should have attribute ${item} but it is not present"
      ...    ELSE     Should not Contain  @{MyList}   ${item}   log  The request should not have attribute ${item}  but is present
    \  log to console  \n"The object verified in response is:"${item}

    # Validate if specific attribute session exists
    run keyword if  '${specificParameterExists}' == 'true'      log to console  The ${specificParameterExists} should exists in response.
    ...     ELSE        Should not Contain  @{MyList}  ${specificParameterExists}   log  "The Parameter" ${specificParameterExists} should not exist but is present in response.
    log to console  \n"The attribute verified in response is:"${specificParameterExists}

   # Validate Response values
    ${Res_ValidateObjectValue}      Get Value From Json          ${Json_response}         ${ValidateObject}
    ${actualvalue}       Get From List       ${Res_ValidateObjectValue}   0

    log to console  "The actual value is"${actualvalue}
    log to console   "The expected object value is":${ExpectedObjectValue}

    should be equal as strings   ${ExpectedObjectValue}  ${actualvalue}    "The value is not as expected"


# To check working
Validate Valid Post Request Response Parameters and Values
    [Arguments]     ${responseJsonPath}    ${validateParameter}      ${expectedParameterValue}      ${shouldNotExistParameter}=null     @{shouldExistresponseParameterList}

    # Load response from file to variable.
    ${Json_response}      Load Json From File        ${responseJsonPath}
    log to console  "The response loaded from file is:"${Json_response}

    # Get the data to list to validate parameter contains in response.
    @{MyList}=  convert to list     ${Json_response}

    #log to console  the list length is:${ength}
    log to console  the list is @{shouldExistresponseParameterList}

    :FOR    ${item}    IN    @{shouldExistresponseParameterList}
    \  Should Contain  @{MyList}   ${item}       log to console  The request should have attribute ${item}  but is present
    \  log to console  \n"The attribute verified in response is:"${item}

    # Validate if should not exist parameter list is provided attribute session exists
    run keyword if  '${shouldNotExistParameter}' == 'null'      log to console  No parameter provided to check non existence in response.
    ...     ELSE        Should not Contain  @{MyList}  ${shouldNotExistParameter}   log  "The Parameter" ${shouldNotExistParameter} should not exist but is present in response.
    log to console  \nThe response is verified with attribute ${shouldNotExistParameter} for non existence

   # Validate Response values
    ${Res_ValidateObjectValue}      Get Value From Json          ${Json_response}         ${validateParameter}
    ${actualvalueOfParameter}       Get From List       ${Res_ValidateObjectValue}   0

    log to console  The actual value of ${validateParameter} is ${actualvalueOfParameter}
    log to console  The expected value of ${validateParameter} is ${expectedParameterValue}

    should be equal as strings   ${expectedParameterValue}  ${actualvalueOfParameter}    The value is not as expected Expected Value:${expectedParameterValue} Actual Value: ${actualvalueOfParameter}

