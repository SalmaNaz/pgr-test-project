*** Settings ***
Library    BuiltIn
Library    Collections
Resource    ../Test Utils/CommonVariables.robot
Resource    ../RestAPIHelper.robot
Resource    ../Test Utils/Test Helper Keywords/PetAPI_Keywords.robot

*** Variables ***
@{noisePetNameList}     d.o.g   D.O.G.  n/a     .   D-o-g   Unknown     D.o.G   d.o.g.  N/A     UNKNOWN
@{patternisticPetNameNoiselist}   delete    deseasedTestPet     Test1234Pet     TestPet#&*#     shitas  willpetbite appointment     hair cut   testing

*** Test Cases ***
# ******************** THIS SUITE INCLUDES THE CREATE PET API TESTCASES *************************************

# This file allows you to create Pet API Testcases.
# We have multiple configurable parameters. We can pass the parameters per test case need.
# Below are the parameters available in "GeneratePetAPIData SubmitRequest And Validate Valid Response" keyword and expect values as shown below
# ${expectedStatusCode}: This is required parameter and expect valid response codes. Ex: 200, 400, 404
# ${validateParameter}: This is required parameter and expect valid attribute which needs to be verified in response. Ex: $..name,  $..pet_color,  $..neutered
# ${ValidateParameterValue}= This is optional parameter and expect value of attribute passed in ${validateParameter}.
# Ex:No need to pass when random data is generated. The value will be picked from data generated dictionary else provide the expected value from test case.
# ${deleteParameter}= This is optional parameter and expect valid attribute which needs to be deleted from json.Ex: $..name,  $..pet_color,  $..neutered
# ${shouldNotExistParameter}= This is optional parameter and expect value when delete parameter is provided. The attribute which is deleted shouold not exist in response. Ex:shouldNotExistParameter=pet_color
# ${apikey}= This is optional parameter and expect api key value. By default it is mobile api key. Ex: ${apikey}=&{apikeyDict}[ecomm]
# ${petParentId}, ${petName}, ${petColor}, ${petType}, ${petBreed}, ${petGender}, ${petWeight}, ${petDeceasedFlag}, ${petNeuteredFlag}:
# These are optional parameters and expect when data when specific value needs to be set else random data will be generated and is used for test execution.

[PetAPI_TC_01] Verify Pet API Post with Valid Pet name
  [Tags]
  [Documentation]    Expected Result - Response Code : 200. Pet should be created successfully and Pet name should be found in response
  GeneratePetAPIData SubmitRequest And Validate Valid Response  ${ResponseCodeOK}     ${petName_jsonPath}

[PetAPI_TC_02] Verify Pet API Post with Empty Pet name
  [Tags]
  [Documentation]     Expected Result - Response Code : 400. Pet should not be created and response should have message {"name": ['is considered as a required field']}
  GeneratePetAPIData SubmitRequest And Validate Valid Response  ${ResponseCodeBadRequest}  ${petName_jsonPath}  ['is considered as a required field']  petName=${EMPTY}

PetAPI_TC_03 Verify Pet API Post with Symbol Pet name
  [Tags]
  [Documentation]     Expected Result - Response Code : 400. Pet should not be created and response should have message {"name": ['Testpet*?$#% is considered as a noise word']}
  GeneratePetAPIData SubmitRequest And Validate Valid Response   ${ResponseCodeBadRequest}   ${petName_jsonPath}  ['Testpet*?$#% is considered as a noise word']  petName=Testpet*?$#%

PetAPI_TC_04 Verify Pet Post API with Noise Pet name
  [Tags]
  [Documentation]     Expected Result - Response Code : 400. Pet should not be created and response should have message {"name": ['Testpet*?$#% is considered as a noise word']}
  :For  ${item}     IN      @{noisePetNameList}
  \  GeneratePetAPIData SubmitRequest And Validate Valid Response   ${ResponseCodeBadRequest}   ${petName_jsonPath}  ['${item} is considered as a noise word']  petName=${item}

PetAPI_TC_05 Verify Pet API Post with patternistic Noise Pet name
  [Tags]
  [Documentation]     Expected Result - Response Code : 400. Pet should not be created and response should have message {"name": ['Testpet*?$#% is considered as a noise word']}
  :For  ${item}     IN      @{patternisticPetNameNoiselist}
  \  GeneratePetAPIData SubmitRequest And Validate Valid Response   ${ResponseCodeBadRequest}   ${petName_jsonPath}  ['${item} is considered as a noise word']  petName=${item}

[PetAPI_TC_06] Verify Pet API Post with Valid value Pet color
  [Tags]
  [Documentation]    Expected Result - Response Code : 200. Pet should be created successfully and given valid Pet color "White" should be found in response
  GeneratePetAPIData SubmitRequest And Validate Valid Response  ${ResponseCodeOK}     ${petColor_jsonpath}      petcolor=White

[PetAPI_TC_06] Verify Pet API Post with Unknown value to Pet color
  [Tags]
  [Documentation]    Expected Result - Response Code : 200. Pet should be created successfully and given valid Pet color "Unknown" should be found in response
  GeneratePetAPIData SubmitRequest And Validate Valid Response  ${ResponseCodeOK}     ${petColor_jsonpath}   petColor=Unknown

[PetAPI_TC_06] Verify Pet API Post without Pet color
  [Tags]
  [Documentation]    Expected Result - Response Code : 200. Pet should be created successfully without pet color attribute in  response.
  GeneratePetAPIData SubmitRequest And Validate Valid Response  ${responsecodeok}     ${petName_jsonPath}  deleteParameter=${petColor_jsonpath}     shouldNotExistParameter=pet_color
